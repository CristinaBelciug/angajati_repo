package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    private Employee e;
    private EmployeeMock employeeMock;
    private int employeenNumber;

    @BeforeEach
    void setUp() {
        e=new Employee();
        e.setId(1);
        e.setFirstName("Daniela");
        e.setLastName("Varga");
        e.setCnp("2840505050229");
        e.setFunction(DidacticFunction.TEACHER);
        e.setSalary(4500d);
        employeeMock = new EmployeeMock();
        try {
            employeenNumber = employeeMock.getEmployeeList().size();
        }
        catch(Exception e)
        {
        }
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }


    @Test
    void addEmployeeTC1() {
        e.setFirstName("Aneiela");
        e.setSalary(4500d);
        boolean isvalid = employeeMock.addEmployee(e);
        assertTrue (isvalid);
    }

    @Test
    void addEmployeeTC2() {
        e.setLastName("Tomescu");
        e.setSalary(4500d);
        boolean isvalid = employeeMock.addEmployee(e);
        assertTrue (isvalid);
    }
    @Test
    void addEmployeeTC8() {
        e.setSalary(0.0);
        e.setFirstName("Miruna");
        boolean isvalid = employeeMock.addEmployee(e);
        assertFalse(isvalid);

    }
    @Test
    void addEmployeeTC14() {
        e.setFirstName("da");
        e.setSalary(4500.0);
        boolean isvalid = employeeMock.addEmployee(e);
        assertFalse(isvalid);

    }

    @Test
    void addEmployeeTC15() {
        e.setLastName("BE");
        e.setSalary(4500.0);
        boolean isvalid = employeeMock.addEmployee(e);
        assertFalse(isvalid);
    }


}