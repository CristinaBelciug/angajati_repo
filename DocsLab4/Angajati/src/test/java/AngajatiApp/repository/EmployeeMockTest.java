package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    private Employee e;
    private EmployeeMock employeeMock;

    @BeforeEach
    void setUp() {
        e = new Employee();
        e.setId(6);
        employeeMock = new EmployeeMock();
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void modifyP03EmployeeFunction() {
        e.setId(7);
        employeeMock.modifyEmployeeFunction(e, DidacticFunction.LECTURER);
        for (Employee searchedEmployee : employeeMock.getEmployeeList()) {
            if (searchedEmployee.getId() == e.getId()) {
                assert (false);
            }
        }
    }

    @Test
    void modifyP04EmployeeFunction() {
        employeeMock.modifyEmployeeFunction(e, DidacticFunction.LECTURER);
        for(Employee searchedEmployee : employeeMock.getEmployeeList())
        {
            if(searchedEmployee.getId() == e.getId())
            {
                assertEquals(searchedEmployee.getFunction(),DidacticFunction.LECTURER);
            }
        }
    }



    @Test
    void modifyP01EmployeeFunction() {
        final boolean b = e == null;
        employeeMock.modifyEmployeeFunction(e, DidacticFunction.LECTURER);
    assertFalse(b);
    }
    @Test
    void modifyP01EmployeeFunctionVersiuneCorectata() {
        //  e = null;
          employeeMock.modifyEmployeeFunction(e, DidacticFunction.TEACHER);

        for(Employee searchedEmployee : employeeMock.getEmployeeList()){
            if (searchedEmployee == null) {
                assert (false);
            }
        }
    }



}